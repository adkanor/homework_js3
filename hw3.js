// 1. Циклы - это возможность сделать одно и то же действие несколько раз, не дублируя код, проверять корректность введеной информации и т.д.
// 2. Цикл for я буду использовать в том случае, если знаю точное кол-во итераций, которое я хочу что бы выполнил цикл.
// Цикл wile  я буду использовать если хочу что бы условия цикла проверялись каждый раз,
//  когда значение меня не устраивает и  была возможность менять информацию столько раз,
//   пока условия цикла не будут подходить под условия определенной задачи.
//  Цикл do while я буду использовать, если мне нужно что бы цикл выполнился хотя бы один раз, а после этого сделал проверку  условия, которое будет описано в while.
// 3. Явное преобразоание типов - когда разработчик специально конвертирует один тип значения в другое,
//  а не явное превращение типов - это когда разработчик делает напрямую другую задачу , но как следствие меняются типы некоторых значений.
("use strict");

// Exeecise 1

// let number;
// do {
//   number = +prompt("Write any number", 10);
//   if (number < 5) {
//     alert("Sorry, no numbers");
//   } else {
//     for (let i = 0; i <= number; i++) {
//       if (i % 5 === 0) {
//         console.log(i);
//       }
//     }
//   }
// } while (!Number.isInteger(number));

// Exeecise 2

let m = Number(prompt("Write any number", 1));
let n = Number(prompt("Write any number bigger than first one", 112));
let checkNumbers;

while (isNaN(n) || isNaN(m) || m > n) {
  m = Number(prompt("Write any number", 1));
  n = Number(prompt("Write any number bigger than first one", 112));
}
for (let number = m; number <= n; number++) {
  let isSimple = true;
  for (let i = 2; i <= 9 && isSimple; i++) {
    isSimple = number % i !== 0 || number / i === 1;
  }
  if (isSimple) {
    console.log(number);
  }
}
